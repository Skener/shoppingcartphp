<?php

session_start();
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (isset($_POST['add']) && $_POST['add'] == 'AddToBasket' && !empty($_POST['product-name'])):
        $products [] = [
            'product-name' => $_POST['product-name'],
            'product-qty' => $_POST['product-qty'],
            'product-price' => $_POST['product-price']
        ];
        $_SESSION['products'] [] = $products;
    endif;
    if (isset($_POST['rm']) && $_POST['rm'] == 'EmptyBasket'):
        $_SESSION['products'] = [];
    endif;
}

?>
<div>
    <fieldset>
        <legend>Add products to basket</legend>
        <form action="" method="post">
            <input type="text" name="product-name">
            <input type="text" name="product-qty" maxlength="2" size="3">
            <input type="text" name="product-price" maxlength="2" size="3">
            <input type="submit" name="add" value="AddToBasket">
            <input type="submit" name="rm" value="EmptyBasket">
        </form>
    </fieldset>
    <p></p>
    <fieldset>
        <legend>Products List in Basket</legend>
        <table cellspacing="0" border="1" cellpadding="5" width="600">
            <tr align="center">
                <th>Product Name</th>
                <th>Q-ty</th>
                <th>Price</th>
                <th>Items Price</th>
                <th>Total Price</th>
            </tr>
            <?php
            $products_items = $_SESSION['products'] ?? null;
            $total = 0;
            $items_price = 0;
            if (!empty($products_items)):
                foreach ($products_items as $key => $item):
            if (!empty($item[0]['product-name']) && !empty($item[0]['product-qty']) && !empty($item[0]['product-price'])):
                    echo '<tr align="center"><td>';
                    $productName = $item[0]['product-name'];
                    echo $productName;
                    echo '</td>';
                    echo '<td>';
                        $qty = $item[0]['product-qty'];
                        echo $qty;
                    echo '</td>';
                    echo '<td>';
                        $price = $item[0]['product-price'];
                        echo $price;
                    echo '</td>';
                    echo '<td>';
                    $items_price = (int)$item[0]['product-qty'] * (int)$item[0]['product-price'];
                    $total += $items_price;
                    echo $items_price;
                    echo '</td></tr>';
                    endif;
                endforeach;
            endif;
            echo '<tr><td colspan="5" align="right">';
            echo '<h2>' . $total . '</h2>';
            echo '</td></tr>';
            ?>
        </table>
</div>



