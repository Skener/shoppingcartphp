var inventory = [{
    "category": "ХОЛОДНІ ЗАКУСКИ:",
    "product": [{
        "id": 1,
        "details": "Сирне асорті",
        "photo": "3.jpg",
        "weight": 0.16,
        "price": 130
    }, {
        "id": 2,
        "details": "Мясне асорті",
        "photo": "4.jpg",
        "weight": 0.2,
        "price": 231
    },
        {
            "id": 10,
            "details": "Асорті з сала з грінками",
            "photo": "4.jpg",
            "weight": 0.15,
            "price": 75
        },
        {
            "id": 11,
            "details": "Заливне з язика",
            "photo": "4.jpg",
            "weight": 0.1,
            "price": 48
        },
        {
            "id": 12,
            "details": "Короп фарширований",
            "photo": "4.jpg",
            "weight": 0.1,
            "price": 53.5
        },
        {
            "id": 3,
            "details": "Асорті з різносолів",
            "photo": "6.jpg",
            "weight": 0.3,
            "price": 85
        }, {
            "id": 4,
            "details": "Овочеве асорті",
            "photo": "3.jpg",
            "weight": 0.3,
            "price": 80
        }, {
            "id": 5,
            "details": "Рибне плато",
            "photo": "6.jpg",
            "weight": 0.21,
            "price": 385
        },
        {
            "id": 12,
            "details": "Оселедець власного маринаду з овочами",
            "photo": "6.jpg",
            "weight": 0.1,
            "price": 84
        },
        {
            "id": 6,
            "details": "Пивна закуска",
            "photo": "3.jpg",
            "weight": 0.12,
            "price": 115
        }]
}, {
    "category": "САЛАТИ:",

    "product": [{
        "id": 7,
        "details": "Цезар з куркою",
        "photo": "3.jpg",
        "weight": 0.25,
        "price": 157
    }, {
        "id": 8,
        "details": "Грецький салат",
        "photo": "3.jpg",
        "weight": 0.25,
        "price": 85
    },
        {
            "id": 9,
            "details": "Салат з куркою",
            "photo": "3.jpg",
            "weight": 0.25,
            "price": 79
        },
        {
            "id": 20,
            "details": "Цезар з лососем",
            "photo": "3.jpg",
            "weight": 0.25,
            "price": 235
        },
        {
            "id": 13,
            "details": "Салат теплий з качиним філе",
            "photo": "3.jpg",
            "weight": 0.24,
            "price": 185
        },
        {
            "id": 9,
            "details": "Цезар з куркою",
            "photo": "3.jpg",
            "weight": 0.25,
            "price": 157
        }]
},
    {
        "category": "ШАШЛИКИ:",

        "product": [{
            "id": 14,
            "details": "Шашлик зі свинної шиї",
            "photo": "3.jpg",
            "weight": 0.1,
            "price": 67
        }, {
            "id": 15,
            "details": "Шашлик зі свинної полядвиці",
            "photo": "3.jpg",
            "weight": 0.1,
            "price": 67
        }, {
            "id": 16,
            "details": "Шашлик зі свинної вирізки",
            "photo": "3.jpg",
            "weight": 0.1,
            "price": 75
        }]
    },

    {
        "category": "ГАРНІРИ НА МАНГАЛІ:",
        "product": [{
            "id": 17,
            "details": "Печериці",
            "photo": "3.jpg",
            "weight": 0.1,
            "price": 33
        }, {
            "id": 18,
            "details": "Баклажани",
            "photo": "3.jpg",
            "weight": 0.1,
            "price": 39
        }, {
            "id": 19,
            "details": "Болгарський перець",
            "photo": "3.jpg",
            "weight": 0.1,
            "price": 39
        }]
    },

    {
        "category": "ГАРНІРИ:",

        "product": [{
            "id": 21,
            "details": "Картопля гратен",
            "photo": "3.jpg",
            "weight": 0.1,
            "price": 33
        }, {
            "id": 22,
            "details": "Рис відварний",
            "photo": "3.jpg",
            "weight": 0.15,
            "price": 40
        }, {
            "id": 23,
            "details": "Болгарський перець",
            "photo": "3.jpg",
            "weight": 0.1,
            "price": 39
        }]
    },

    {
        "category": "СОУСИ:",

        "product": [{
            "id": 40,
            "details": "Соус аджика червона",
            "photo": "3.jpg",
            "weight": 0.05,
            "price": 25
        }, {
            "id": 41,
            "details": "Соус сацебелі",
            "photo": "3.jpg",
            "weight": 0.05,
            "price": 25
        }, {
            "id": 42,
            "details": "соус Ткемалі червоний(з аличі)",
            "photo": "3.jpg",
            "weight": 0.05,
            "price": 25
        }]
    }];

