var products = inventory;
var row = document.getElementById('prd_content');
products.forEach(function (item) {
    row.innerHTML +=
        `<tr>
    <td colspan="6"><h3>${item.category}</h3></td>
    </tr>`;
    item.product.forEach(function (prod, i) {
        row.innerHTML +=
            `<tr class="prd_row">
    <td name="id" value="${prod.id}">${i + 1}</td>
    <td name="name">${prod.details}</td>
    <td name="photo">${prod.photo}</td>
    <td name="weight">${prod.weight}</td>
    <td name="price">${prod.price}</td>
    <td><span class="btn-add">Add +</span></td>
    </tr>`;
    })
});

var shop_basket = [];
var prdTable = document.querySelector('#prd_content');
prdTable.addEventListener('click', (e) => {
    if (e.target.className === 'btn-add') {
        var trow = e.target.parentElement.parentElement;
        var itemId = trow.children.id.innerText;
        var itemName = trow.children.name.innerText;
        var itemPhoto = trow.children.photo.innerText;
        var itemWeight = parseFloat(trow.children.weight.innerText);
        var itemPrice = parseInt(trow.children.price.innerText);
        itemQty = 1;
        for (var i in shop_basket) {
            var basket_item = shop_basket[i];
            if (itemId === basket_item.id) {
                itemQty = parseInt(basket_item.qty) + 1;
                itemWeight += parseFloat(basket_item.weight);
                itemInCart = shop_basket.indexOf(basket_item);
                shop_basket.splice(itemInCart, 1);
            }
        }
        var item_to_add = {
            id: itemId,
            name: itemName,
            photo: itemPhoto,
            weight: itemWeight.toFixed(2),
            price: itemPrice,
            qty: itemQty
        };
        shop_basket.push(item_to_add);

        // Generate random Order ID
        var OrderID = function () {

            return 'od' + Date.now().toString(5);

            // return id;
        };

        var order_id = OrderID();

        // Print function
        function printMe(element) {
            var printContent = element.innerHTML;
            var originalContent = window.document.body.innerHTML;
            window.document.body.innerHTML = printContent;
            window.print();
            window.document.body.innerHTML = originalContent;
        }

        // Order TimeStamp
        var currentDate = new Date();
        var date = currentDate.getDate();
        var month = currentDate.getMonth();
        var year = currentDate.getFullYear()
        var hour = currentDate.getHours();
        var min = currentDate.getMinutes();
        var monthDateYear = (hour) + ":" + (min) + " " + (month + 1) + "-" + date + "-" + year;
        console.log(monthDateYear);
        console.log(order_id);

        // Show Order
        setTimeout(function () {
            var tbody = document.getElementById('order-busket');
            tbody.innerHTML = '';
            shop_basket.forEach(function (product, i) {
                tbody.innerHTML +=
                    `<tr>
   <td name="prd_id" value="${product.id}">${i + 1}</td>
   <td> ${product.name}</td>
    <td>${product.photo}</td>
    <td>${product.weight}</td>
    <td>${product.price}</td>
    <td>${product.qty}</td>
    </tr>`;
            });
            var totalCost = 0;
            for (var idx in shop_basket) {
                var currentItem = shop_basket[idx];
                totalCost += parseInt(currentItem.qty) * currentItem.price;
            }
            var total = document.querySelector('.total-cost');
            total.innerHTML = `<strong>Всього: ${totalCost}</strong>`;
        }, 550);
    }
}, true);